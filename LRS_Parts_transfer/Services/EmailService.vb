﻿Imports System.Net
Imports System.Net.Mail

Namespace Utilities
    Public Class EmailService
        Public Shared Sub SendMail(message As MailMessage)
            Try
                Dim smtpServer As New SmtpClient
                smtpServer.UseDefaultCredentials = False
                smtpServer.Credentials = New NetworkCredential("noreply@vmgsoftware.co.za", "123NoReply+321")
                smtpServer.Port = 25
                smtpServer.EnableSsl = False
                smtpServer.Host = "mail.vmgsoftware.co.za"
                smtpServer.Timeout = 60000


                ' Default message to sending from no reply
                If IsNothing(message.From) Then
                    message.From = New MailAddress("noreply@vmgsoftware.co.za", "LRS Parts Transfer")
                End If

                ' Change the email address if this is a developer company id

                message.To.Clear()
                message.To.Add("stephen@vmgsoftware.co.za")
                'message.CC.Add("justin@vmgsoftware.co.za")


                smtpServer.Send(message)

            Catch ex As Exception
                Throw
            End Try

        End Sub
    End Class
End Namespace
