﻿Imports System.Data.SqlClient
Imports System.Linq
Imports System.Threading.Tasks
Imports NLog

Public Class MssqlDatabaseHelper
    Implements IDisposable
    Private ReadOnly _connectionString As String
    Private ReadOnly _ctx As SqlConnection = Nothing
    Private ReadOnly _logger As ILogger = LogManager.GetCurrentClassLogger()

    Public Sub New(connectionString As String)
        _connectionString = connectionString
        _ctx = New SqlConnection(_connectionString)
    End Sub

    Public Sub New(connection As SqlConnection)
        _connectionString = connection.ConnectionString
        _ctx = connection
    End Sub

    Public Function GetConnection() As SqlConnection
        Return _ctx
    End Function

    Public Function TestDbConnection() As SqlException
        Dim canOpen As Boolean = False
        Try
            OpenConnection()
            canOpen = _ctx.State = ConnectionState.Open
        Catch ex As SqlException
            Throw ex
        End Try

        Return Nothing
    End Function


    Public Function GetSqlStringCommand(sql As String) As SqlCommand
        Dim cmd As SqlCommand = GetConnection().CreateCommand()
        cmd.CommandText = sql
        Return cmd
    End Function

    Private Sub ResetConnection()
        If (_ctx.State <> ConnectionState.Closed) Then
            _ctx.Close()
        End If
    End Sub

    Private Sub ResetConnection(ctx As SqlConnection)
        If (ctx.State <> ConnectionState.Closed) Then
            ctx.Close()
        End If
    End Sub

    Private Sub OpenConnection()
        ResetConnection()
        _ctx.Open()
    End Sub

    Private Sub OpenConnection(ctx As SqlConnection)
        ResetConnection(ctx)
        ctx.Open()
    End Sub

    ' Basics
    'Getting data out
    Public Function ExecuteReader(cmd As String) As SqlDataReader

        Return ExecuteReader(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteReader(cmd As SqlCommand) As SqlDataReader
        OpenConnection(cmd.Connection)
        Return cmd.ExecuteReader(CommandBehavior.CloseConnection)
    End Function

    Public Function ExecuteScalar(cmd As String) As Object
        Return ExecuteScalar(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteScalar(cmd As SqlCommand) As Object
        OpenConnection(cmd.Connection)
        Dim result As Object = cmd.ExecuteScalar()
        ResetConnection(cmd.Connection)
        Return result
    End Function

    ' Putting data in
    Public Function ExecuteNonQuery(cmd As String) As Integer
        Return ExecuteNonQuery(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteNonQuery(cmd As SqlCommand) As Integer
        If cmd.Connection Is Nothing Then
            cmd.Connection = GetConnection()
        End If
        OpenConnection(cmd.Connection)
        Dim result As Integer = cmd.ExecuteNonQuery()
        ResetConnection(cmd.Connection)
        Return result
    End Function

    ' More Interesting
    Public Function ExecuteDataSet(cmd As String) As DataSet
        Return ExecuteDataSet(GetSqlStringCommand(cmd))
    End Function

    Private Function ExecuteDataSet(cmd As SqlCommand) As DataSet
        Return ExecuteDataSet(cmd, "dataTable")
    End Function

    Public Function ExecuteDataSet(cmd As String, tableName As String) As DataSet
        Return ExecuteDataSet(GetSqlStringCommand(cmd), tableName)
    End Function

    Public Async Function ExecuteDataSetAsync(cmd As String, tableName As String) As Task(Of DataSet)
        Try
            Dim result = Await ExecuteDataSetAsync(GetSqlStringCommand(cmd), tableName)
            Return result
        Catch ex As Exception
            _logger.Error(ex)
            Throw
        End Try
    End Function

    Private Function ExecuteDataSet(cmd As SqlCommand, tableName As String) As DataSet
        OpenConnection(cmd.Connection)
        Dim dataSet As DataSet = New DataSet()
        Using dataAdapter As SqlDataAdapter = New SqlDataAdapter(cmd)
            dataAdapter.Fill(dataSet, tableName)
        End Using
        Return dataSet
    End Function

    Private Function ExecuteDataSetAsync(cmd As SqlCommand, tableName As String) As Task(Of DataSet)
        Dim tcs = New TaskCompletionSource(Of DataSet)

        Try
            OpenConnection(cmd.Connection)
            Dim dataSet As DataSet = New DataSet()
            Using dataAdapter As SqlDataAdapter = New SqlDataAdapter(cmd)
                dataAdapter.Fill(dataSet, tableName)
            End Using

            tcs.TrySetResult(dataSet)

        Catch ex As Exception
            tcs.TrySetException(ex)
        End Try

        Return tcs.Task

    End Function

    Public Function ExecuteStoredProcedure(cmd As String) As Integer
        Return ExecuteStoredProcedure(GetSqlStringCommand(cmd))
    End Function

    Private Function ExecuteStoredProcedure(cmd As SqlCommand) As Integer
        OpenConnection(cmd.Connection)
        Return cmd.ExecuteNonQuery()
    End Function

    Public Function UpdateDataSet(dataSet As DataSet, cmd As String) As Integer
        Return UpdateDataSet(dataSet, GetSqlStringCommand(cmd))
    End Function

    Private Function UpdateDataSet(dataSet As DataSet, cmd As SqlCommand) As Integer
        OpenConnection(cmd.Connection)
        Dim rowsUpdated As Integer = 0
        Using dataAdapter As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim commandBuilder As SqlCommandBuilder = New SqlCommandBuilder(dataAdapter)
            rowsUpdated = dataAdapter.Update(dataSet)
        End Using
        Return rowsUpdated
    End Function

    Public Sub CloseConnection()

        If (GetConnection.State <> ConnectionState.Closed) Then
            GetConnection.Close()
        End If
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        _ctx?.Dispose()
    End Sub
End Class
