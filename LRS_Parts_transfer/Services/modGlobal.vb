﻿Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports LRS_Parts_transfer.Utilities

Module modGlobal
    Public WithEvents MyFtpUploadWebClient As New WebClient
    Public Async Function FileUploader(ftpAddress As String, filePath As String, username As String, password As String,
                            remoteFileName As String, directoryName As String) As Task 'Create FTP request
        Try
            'Subroutine to upload a file to a remote ftp server
            MyFtpUploadWebClient = New WebClient
            MyFtpUploadWebClient.Proxy = Nothing

            'Uri has to have first argument as full path with file name and file extension (like .html or something). 2nd parameter is pure file name, without the path
            Dim myUri As New Uri(ftpAddress & directoryName & "/" & remoteFileName)

            MyFtpUploadWebClient.Credentials = New NetworkCredential(username, password)
            'Now you do the uploading
            MyFtpUploadWebClient.UploadFileAsync(myUri, filePath)

            Await Task.Delay(1)

        Catch ex As Exception
            WriteErrorLogFile("Error on FileUploader", ex)
        End Try
    End Function

    Public Function EncodeString(str As String) As String
        'replace all single quotes with double quotes and prep string for use in sql query as string parameter
        'USE SQL PARAMETERS AND NOT THIS FUNCTION
        Dim strResult As String
        Dim x As Long
        strResult = ""
        For x = 1 To Len(str)
            If Mid$(str, CInt(x), 1) = "'" Then
                strResult = strResult & "''"
            Else
                strResult = strResult & Mid$(str, CInt(x), 1)
            End If
        Next x
        EncodeString = strResult
    End Function

    Public Sub WriteErrorLogFile(message As String, ex As Exception, Optional title As String = "Application Error")

        'check and make the directory if necessary; this is set to look in 
        'the Application folder, you may wish to place the error log in 
        'another location depending upon the user's role and write access to 
        'different Areas of the file system
        Try

            Dim logFilePath As String = AppDomain.CurrentDomain.BaseDirectory + "\log files"
            If Directory.Exists(logFilePath) = False Then
                Directory.CreateDirectory(logFilePath)
            End If
            logFilePath += "\errors.log"

            If File.Exists(logFilePath) = False Then
                File.Create(logFilePath)
            End If


            'check the file
            Dim fileStream As FileStream = New FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim streamWriter As StreamWriter = New StreamWriter(fileStream)
            streamWriter.Close()
            fileStream.Close()

            Dim stkTrace As String = ex.StackTrace
            Dim excMessage As String = ex.Message

            'log it
            Dim fileStream1 As FileStream = New FileStream(logFilePath, FileMode.Append, FileAccess.Write)
            Dim streamWriter1 As StreamWriter = New StreamWriter(fileStream1)
            streamWriter1.Write("Title: " & title & vbCrLf)
            streamWriter1.Write("Message: " & message & vbCrLf)
            streamWriter1.Write("System Message: " & excMessage & vbCrLf)
            streamWriter1.Write("StackTrace: " & stkTrace & vbCrLf)
            streamWriter1.Write("Date/Time: " & DateTime.Now.ToString() & vbCrLf)
            streamWriter1.Write("================================================================================================" & Environment.NewLine & Environment.NewLine)
            streamWriter1.Close()
            fileStream1.Close()

            Dim lversion As String = ""
            If (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) Then
                With System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion
                    lversion = .Major & "." & .Minor & "." & .Build & "." & .Revision
                End With
            End If


            Dim mail As New MailMessage
            mail.To.Add("stephen@vmgsoftware.co.za")
            mail.Subject = "LRS Parts Upload"
            mail.IsBodyHtml = True

            mail.Body = "<h3>Error on LRS Parts Upload</h3><table><tbody><tr><td>Title : </td><td>" & title & "</td></tr>" &
                        "<tr><td>Message : </td><td>" & message & "</td></tr><tr><td>System Message:</td><td>" & excMessage & "</td></tr>" &
                        "<tr><td>Stack Trace:</td><td>" & stkTrace & "</td></tr><tr><td>Date/Time:</td><td>" & DateTime.Now.ToString() & "</td></tr><tr><td>Version:</td><td>" & lversion & "</td></tr></tbody></table>"

            EmailService.SendMail(mail)
        Catch
        End Try

    End Sub


    Public Sub LogFile(method_name As String, desc As String, endline As Boolean, Optional partid As Double = 0)

        'check and make the directory if necessary; this is set to look in 
        'the Application folder, you may wish to place the error log in 
        'another location depending upon the user's role and write access to 
        'different Areas of the file system
        Try

            Dim logFilePath As String = AppDomain.CurrentDomain.BaseDirectory + "\log files"
            If Directory.Exists(logFilePath) = False Then
                Directory.CreateDirectory(logFilePath)
            End If
            logFilePath += $"\{method_name}_{Now.ToString("yyyyMMdd")}.txt"

            If File.Exists(logFilePath) = False Then
                File.Create(logFilePath)
            End If


            'check the file
            Dim fileStream As FileStream = New FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim streamWriter As StreamWriter = New StreamWriter(fileStream)
            streamWriter.Close()
            fileStream.Close()

            'log it
            Dim fileStream1 As FileStream = New FileStream(logFilePath, FileMode.Append, FileAccess.Write)
            Dim streamWriter1 As StreamWriter = New StreamWriter(fileStream1)

            If partid <> 0 Then
                streamWriter1.Write($"Partid : {partid} {vbCrLf}")
            End If
            streamWriter1.Write(desc & ": " & DateTime.Now.ToString() & vbCrLf)
            If endline = True Then
                streamWriter1.Write("================================================================================================" & Environment.NewLine & Environment.NewLine)
            End If

            streamWriter1.Close()
            fileStream1.Close()


        Catch
        End Try

    End Sub
End Module
