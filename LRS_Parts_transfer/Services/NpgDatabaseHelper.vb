﻿Imports System.Data.SqlClient
Imports Npgsql

Public Class NpgDatabaseHelper


    Implements IDisposable
    Private ReadOnly _connectionString As String
    Private ReadOnly _ctx As NpgsqlConnection = Nothing

    Private _logger As NLog.Logger = NLog.LogManager.GetCurrentClassLogger()

    Public Sub New(connectionString As String)
        _connectionString = connectionString
        _ctx = New NpgsqlConnection(_connectionString)
    End Sub

    Public Sub New(connection As NpgsqlConnection)
        _connectionString = connection.ConnectionString
        _ctx = connection
    End Sub

    Public Function TestDbConnection() As NpgsqlException
        Dim canOpen As Boolean = False

        Try
            OpenConnection()
            canOpen = _ctx.State = ConnectionState.Open
        Catch ex As NpgsqlException
            Throw ex
        End Try

        Return Nothing
    End Function

    Public Function GetConnection() As NpgsqlConnection
        Return _ctx
    End Function

    Public Function GetSqlStringCommand(sql As String) As NpgsqlCommand
        Dim cmd As NpgsqlCommand = GetConnection().CreateCommand()
        cmd.CommandText = sql
        Return cmd
    End Function

    Private Sub ResetConnection()
        If (_ctx.State <> ConnectionState.Closed) Then
            _ctx.Close()
        End If
    End Sub

    Private Sub ResetConnection(ctx As NpgsqlConnection)
        If (ctx.State <> ConnectionState.Closed) Then
            ctx.Close()
        End If
    End Sub

    Private Sub OpenConnection()
        ResetConnection()
        _ctx.Open()
    End Sub

    Private Sub OpenConnection(ctx As NpgsqlConnection)
        Try
            ResetConnection(ctx)
            ctx.Open()
        Catch ex As Exception
            _logger.Error(ex)
            Throw ex
        End Try
    End Sub

    ' Basics
    'Getting data out
    Public Function ExecuteReader(cmd As String) As NpgsqlDataReader
        Return ExecuteReader(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteReader(cmd As NpgsqlCommand) As NpgsqlDataReader
        OpenConnection(cmd.Connection)
        Return cmd.ExecuteReader(CommandBehavior.CloseConnection)
    End Function

    Public Function ExecuteScalar(cmd As String) As Object
        Return ExecuteScalar(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteScalar(cmd As NpgsqlCommand) As Object
        OpenConnection(cmd.Connection)
        Dim result As Object = cmd.ExecuteScalar()
        ResetConnection(cmd.Connection)
        Return result
    End Function

    ' Putting data in
    Public Function ExecuteNonQuery(cmd As String) As Integer
        Return ExecuteNonQuery(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteNonQuery(cmd As NpgsqlCommand) As Integer
        OpenConnection(cmd.Connection)
        Dim result As Integer = cmd.ExecuteNonQuery()
        ResetConnection(cmd.Connection)
        Return result
    End Function

    ' More Interesting
    Public Function ExecuteDataSet(cmd As String) As DataSet
        Return ExecuteDataSet(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteDataSet(cmd As NpgsqlCommand) As DataSet
        Return ExecuteDataSet(cmd, "dataTable")
    End Function

    Public Function ExecuteDataSet(cmd As String, tableName As String) As DataSet
        Return ExecuteDataSet(GetSqlStringCommand(cmd), tableName)
    End Function

    Public Function ExecuteDataSet(cmd As NpgsqlCommand, tableName As String) As DataSet
        OpenConnection(cmd.Connection)
        Dim dataSet As DataSet = New DataSet()
        Using dataAdapter As NpgsqlDataAdapter = New NpgsqlDataAdapter(cmd)
            dataAdapter.Fill(dataSet, tableName)
        End Using
        Return dataSet
    End Function

    Public Function ExecuteStoredProcedure(cmd As String) As Integer
        Return ExecuteStoredProcedure(GetSqlStringCommand(cmd))
    End Function

    Public Function ExecuteStoredProcedure(cmd As NpgsqlCommand) As Integer
        OpenConnection(cmd.Connection)
        Return cmd.ExecuteNonQuery()
    End Function

    Public Function UpdateDataSet(dataSet As DataSet, cmd As String) As Integer
        Return UpdateDataSet(dataSet, GetSqlStringCommand(cmd))
    End Function

    Public Function UpdateDataSet(dataSet As DataSet, cmd As NpgsqlCommand) As Integer
        OpenConnection(cmd.Connection)
        Dim rowsUpdated As Integer = 0
        Using dataAdapter As NpgsqlDataAdapter = New NpgsqlDataAdapter(cmd)
            Dim commandBuilder As NpgsqlCommandBuilder = New NpgsqlCommandBuilder(dataAdapter)
            rowsUpdated = dataAdapter.Update(dataSet)
        End Using
        Return rowsUpdated
    End Function

    Public Sub CloseConnection()

        If (GetConnection.State <> ConnectionState.Closed) Then
            GetConnection.Close()
        End If
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        _ctx?.Dispose()
    End Sub
End Class
