﻿Imports System.IO
Imports System.Net.Mail
Imports LRS_Parts_transfer.Utilities
Imports Npgsql
Imports System.Management


Public Class TransferService
    ReadOnly _serviceName As String = "Parts Transfer"
    Public MyTimer As New Timers.Timer
    'Public right_now As Date
    'Public look_at_the_time As String = " 03:00:01"
    'Public time_to_run As Date
    'Public counter As Double
    Public LocalConnectionString As String
    Public OnlineConnectionString As String

    Public WithEvents Query As New ManagementEventWatcher(New WqlEventQuery("__InstanceModificationEvent", New System.TimeSpan(0, 0, 1) _
                                                                            , "TargetInstance isa 'Win32_LocalTime' AND TargetInstance.Hour=15 AND TargetInstance.Minute=50 AND TargetInstance.Second=00"))

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        AutoLog = True
        If Not EventLog.SourceExists(_serviceName) Then
            EventLog.CreateEventSource(_serviceName, "Event Log")
        End If

        eventLog1.Source = _serviceName

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        eventLog1.WriteEntry($"{_serviceName} started")

        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
        mail.Subject = $"{_serviceName} error"
        mail.IsBodyHtml = True

        mail.Body = $"{_serviceName} Service has started"

        EmailService.SendMail(mail)



        get_settings()
        Dim tempArray As String() = Split(orginal_online, ":", 2)
        Dim portNumber As String
        portNumber = tempArray(1)
        Dim ipAddress As String
        ipAddress = tempArray(0)

        LocalConnectionString = $"data source={orginal_local};initial catalog=amgauto;persist security info=False;packet size=4096;user id = amguser;password = autoAUTO2275!"
        OnlineConnectionString = $"host={ipAddress};database=parts;username =lrs_parts;password =R6ntpo8_yQukF+D@fidEq#*GYwvB-49^Jcv73Xxs9gbrRW@UMh; port={portNumber};Pooling=false"



        AddHandler Query.EventArrived, AddressOf ticktock

        Query.Start()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        eventLog1.WriteEntry($"{_serviceName} stopped")

        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
        mail.Subject = $"{_serviceName} error"
        mail.IsBodyHtml = True

        mail.Body = $"{_serviceName} Service has stopped"

        EmailService.SendMail(mail)
    End Sub

    Protected Overrides Sub OnPause()
        eventLog1.WriteEntry($"{_serviceName} paused")
    End Sub

    Async Sub Ticktock()
        eventLog1.WriteEntry($"Transfer Started")

        'right_now = Now

        'If right_now < Date.Today & Format(TimeString, look_at_the_time) Then
        '    time_to_run = Date.Today & Format(TimeString, look_at_the_time)
        'ElseIf right_now > Date.Today & Format(TimeString, look_at_the_time) Then
        '    time_to_run = DateAdd(DateInterval.Day, 1, Date.Today) & Format(TimeString, look_at_the_time)
        'End If

        'counter = DateDiff(DateInterval.Second, right_now, time_to_run)
        'If counter = 0 Then
        '    'lblCountDown.Text = "Transfer has started"
        Try

            Dim mail As New MailMessage
            mail.To.Add("stephen@vmgsoftware.co.za")
            mail.Subject = $"{_serviceName} Service"
            mail.IsBodyHtml = True

            mail.Body = $"{_serviceName} is currently running"

            EmailService.SendMail(mail)


            Await Task.WhenAll(transfer_parts(LocalConnectionString, OnlineConnectionString, "parts_v1.parts_temp"))

        Catch ex As Exception
            WriteErrorLogFile("error on transfer", ex)
            eventLog1.WriteEntry($"Transfer Error")
        End Try

        eventLog1.WriteEntry($"Transfer Ended")
        'Else

        'End If

        'pgUpload.Visible = False
    End Sub

    Public Async Function transfer_images() As Task
        LogFile("transfer_images", "Time Started", False)
        Try
            Dim count As Integer = 0

            Dim imagesToMove As String = $"{orginal_image_path}\temp"
            Dim di As New IO.DirectoryInfo(imagesToMove)
            Dim diar1 As IO.FileInfo() = di.GetFiles("*.jpg")
            Dim dra As IO.FileInfo

            For Each dra In diar1
                Await FileUploader("ftp://154.66.197.219", dra.FullName, "VMG", "hb**s>+hLCQR8fe7NE@K", dra.Name, "/main/parts")

                Try
                    If File.Exists($"{orginal_image_path}\images_online\{dra.Name}") Then
                        File.Delete($"{orginal_image_path}\images_online\{dra.Name}")
                    End If

                    Await (Task.Delay(5))
                    File.Copy(dra.FullName, $"{orginal_image_path}\images_online\{dra.Name}")

                    If File.Exists($"{orginal_image_path}\images_online\{dra.Name}") Then
                        File.Delete($"{dra.FullName}")
                    End If

                Catch ex As Exception
                    WriteErrorLogFile("transfer_images error", ex)
                End Try
                count = count + 1

            Next

            LogFile("transfer_images", "Time Ended", True)
        Catch ex As Exception
            WriteErrorLogFile("transfer_images error", ex)
            LogFile("transfer_images", "Error Occurred", True)
        End Try
    End Function

    Public Async Function transfer_parts(local_db As String, online_db As String, tableToInsertInto As String) As Task
        LogFile("transfer_parts", "Time Started", False)
        Dim str As String = ""
        Try

            Dim transaction As NpgsqlTransaction
            Dim command As NpgsqlCommand
            Dim localDb = New MssqlDatabaseHelper(local_db)
            localDb.ExecuteStoredProcedure("SetupCrossReference")
            Dim facet_table As String = "parts_v1.parts_facet_temp"
            Dim quickview As String = ""
            Dim count As Integer
            Dim queryInsert As String = $"INSERT INTO {tableToInsertInto} (  part_id,part_code,  master_code,  part_desc,  selling_price_exc,  brand,  web_notes,  selling_price_inc,  stock_qty,  remove_from_web, unit, quick_view,search_string) values"
            Dim lData As New DataTable

            lData = (Await localDb.ExecuteDataSetAsync($"SELECT  PartID,PCode,MasterCode,PartDesc,SellPriceExc,Brand, WebNotes ,SellPriceInc ,Avail ,0 AS RemoveFromWeb, unit, quick_view, search_string 
                                        FROM ShopDirectPartsOnWeb where SellPriceExc <> 0 ", "local")).Tables("local")

            If lData.Rows.Count > 0 Then

                Using remoteDb = New NpgsqlConnection(online_db)
                    remoteDb.Open()

                    command = New NpgsqlCommand($"truncate {tableToInsertInto} ", remoteDb)
                    Await command.ExecuteNonQueryAsync
                    command = New NpgsqlCommand($"truncate {facet_table} ", remoteDb)
                    Await command.ExecuteNonQueryAsync

                    For Each dataRow As DataRow In lData.Rows
                        If dataRow.Item("quick_view").ToString = "True" Then
                            quickview = "true"
                        Else
                            quickview = "false"
                        End If
                        transaction = remoteDb.BeginTransaction()
                        str = $"{queryInsert}({dataRow.Item("PartID").ToString},'{dataRow.Item("pcode").ToString}','{dataRow.Item("mastercode").ToString}','{EncodeString(dataRow.Item("partdesc").ToString)}',{Replace(dataRow.Item("SellPriceExc").ToString, ",", ".")},'{dataRow.Item("Brand").ToString}','{EncodeString(dataRow.Item("WebNotes").ToString)}',{Replace(dataRow.Item("SellPriceInc").ToString, ",", ".")},0,false,'{dataRow.Item("unit").ToString}',{quickview},'{EncodeString(dataRow.Item("search_string").ToString)}')"
                        command = New NpgsqlCommand(str, remoteDb, transaction)

                        Await command.ExecuteNonQueryAsync
                        Call transfer_facets(local_db, online_db, facet_table, dataRow.Item("PartID").ToString, transaction, remoteDb)

                        count = count + 1
                        Await transaction.CommitAsync()
                        str = transaction.ToString()
                        transaction.Dispose()


                    Next

                    if count <> 0 Then
                        command = New NpgsqlCommand($"select parts_v1.transfer_data()", remoteDb)
                        Await command.ExecuteNonQueryAsync
                    End If
                End Using

            End If
            LogFile("transfer_parts", "Time Ended", True)
        Catch ex As Exception
            WriteErrorLogFile(Str, ex)
            LogFile("transfer_parts", "Error Occurred", True)
        End Try

        'pgUpload.Visible = False
    End Function

    Public Sub transfer_facets(local_db As String, online_db As String, tableToInsertInto As String, part_id As Double, ByRef current_transaction As NpgsqlTransaction, ByRef remotedb As NpgsqlConnection)
        LogFile($"transfer_facets", "Time Started", False, part_id)
        Try

            Dim localDb = New MssqlDatabaseHelper(local_db)
            Dim count As Double = 0
            Dim queryInsert As String = $"INSERT INTO {tableToInsertInto} (  part_id, model, engine_desc, part_group, part_sub_group, part_sub_group_detail) values"
            Dim lData As New DataTable


            lData = localDb.ExecuteDataSet($"Select   PartID, Model, EngineDesc, PartGroup, PartSubGroup, PartSubGroupDetail FROM ShopDirectFacet  WHERE ISNULL(Model,'') <> '' AND ISNULL(PartGroup,'') <> '' and partid = {part_id}", "local").Tables("local")

            If lData.Rows.Count > 0 Then
                For Each dataRow As DataRow In lData.Rows
                    Using command = New NpgsqlCommand($"{queryInsert}({dataRow.Item("PartID").ToString},'{EncodeString(dataRow.Item("model").ToString)}','{EncodeString(dataRow.Item("EngineDesc").ToString)}','{EncodeString(dataRow.Item("PartGroup").ToString)}','{EncodeString(dataRow.Item("PartSubGroup").ToString)}','{EncodeString(dataRow.Item("PartSubGroupDetail").ToString)}')", remotedb, current_transaction)
                        command.ExecuteNonQuery()
                    End Using
                Next
                'current_transaction.CommitAsync()
                'current_transaction.Dispose()

            End If
            LogFile($"transfer_facets", "Time Ended", True)
        Catch ex As Exception
            WriteErrorLogFile("transfer_facets error", ex)
            LogFile($"transfer_facets", "Error Occurred", True)
        End Try
    End Sub
End Class
