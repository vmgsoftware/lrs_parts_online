﻿Imports System.IO
Imports System.Xml


Module modFunctions
    Public orginal_local As String
    Public orginal_online As String
    Public orginal_image_path As String

    Public Sub get_settings()

        Dim file_path As String = AppDomain.CurrentDomain.BaseDirectory
        Dim file_name As String = "settings.xml"

        If File.Exists(file_path & "\" & file_name) = True Then
            Dim m_xmld As XmlDocument
            Dim m_nodelist As XmlNodeList
            Dim m_node As XmlNode
            'Create the XML Document
            m_xmld = New XmlDocument()
            'Load the Xml file
            m_xmld.Load(file_path & "\" & file_name)
            'Get the list of name nodes 
            m_nodelist = m_xmld.SelectNodes("settings")
            'Loop through the nodes
            For Each m_node In m_nodelist
                orginal_local = m_node.ChildNodes.Item(0).InnerText
                orginal_online = m_node.ChildNodes.Item(1).InnerText
                orginal_image_path = m_node.ChildNodes.Item(2).InnerText
            Next
        End If
    End Sub
End Module
